
# Writing image annotation scripts

ImageAnt allows you to define custom image annotation tasks that can be arranged in annotation "scripts" for generating sophisticated annotation data.  This document describes the mini-language for writing new annotation scripts.

  * [Annotation tasks](#annotation-tasks)
  * [Annotation task lists](#annotation-task-lists)
  * [Dependent annotation task lists](#dependent-annotation-task-lists)


## Annotation tasks

Image annotation tasks are defined by specifying, at a minimum, 1) a text string to display to the user, 2) a set of possible responses, and 3) the name of a variable in which to record the user's response.  To make this more concrete, let's look at an example.  Here, we'd like the user to examine photographs of plants and decide whether each image shows a plant with flowers.  The task definition might look like this:
```yaml
- text: 'Does the image show a plant with flowers?'
  variable: 'flowers'
  responses:
    - text: '"p": flowers present'
      action: keypress{p ~ Y}
    - text: '"a": flowers absent'
      action: keypress{a ~ N}
```

For each image, the user will be shown a prompt asking `Does the image show a plant with flowers?`, followed by the two possible responses: `"p": flowers present` and `"a": flowers absent`.  The actions the user can take are defined by the `action` statements in each response.  The action `keypress{p ~ Y}` means that when the user presses the `p` key, the value `'Y'` will be recorded as the user's response.  Similarly, to indicate that flowers are absent on an image, the user would press the `a` key, which will record `'N'` as the user's response.  The user's response for each image (i.e., either `'Y'` or `'N'`) will be written to a [CSV file](https://en.wikipedia.org/wiki/Comma-separated_values) that includes a column called `flowers`, which corresponds with the variable name provided in the task definition.

The syntax of the `keypress` directive allows you to add whitespace between elements and enclose key or response values in either single or double quotes, so you can write the `keypress` directives in whatever way is easiest for you to read.  For example, the following variations of the example above work just fine:
```yaml
- text: 'Does the image show a plant with flowers?'
  variable: 'flowers'
  responses:
    - text: '"p": flowers present'
      action: keypress { 'p'~'Y' }
    - text: '"a": flowers absent'
      action: keypress{ "a" ~ "N (no flowers)" }
```

Annotation tasks can be as complex as needed, and so are not limited to binary responses.  For example, suppose we'd like the user to be able to indicate that the information in an image is insufficient to make a decision about whether the photographed plant has flowers.  We could add a third option to the task, like this:
```yaml
- text: 'Does the image show a plant with flowers?'
  variable: 'flowers'
  responses:
    - text: '"p": flowers present'
      action: keypress{p ~ Y}
    - text: '"a": flowers absent'
      action: keypress{a ~ N}
    - text: '"u": uncertain'
      action: keypress{u ~ U}
```

The syntax for defining tasks should be fairly clear from the examples.  Each new task definition should begin with a hypen (`-`), as should each response definition.  Note that indentation _does_ matter; the response definitions must be indented to indicate that they are part of the `responses` section.  Use spaces to indent, not tabs.  The number of spaces to use for indentation does not matter, as long as it is greater than 0.

You can add comments to your task definitions to help document them.  Comments begin with a `#`.  Everything on a line after a `#` will be ignored when the task definitions are parsed.  For example:
```yaml
#
# The first annotation task asks if the image includes a plant with flowers.
#
- text: 'Does the image show a plant with flowers?'
  variable: 'flowers'
  responses:
    - text: '"p": flowers present'
      action: keypress{p ~ Y}
    - text: '"a": flowers absent'
      action: keypress{a ~ N}
    - text: '"u": uncertain'
      action: keypress{u ~ U}
```


## Annotation task lists

Annotation tasks are organized into task lists that group tasks together and define how they are presented to the user.  All tasks must be arranged in task lists.  The syntax for defining a task list is simple.  To create a task list, simply write the name of the task list followed by a colon, then write indented task definitions after the task list name.  To continue our example, let's place the flowers annotation task inside of a task list called `main_tasks`:
```yaml
main_tasks:
  - text: 'Does the image show a plant with flowers?'
    variable: 'flowers'
    responses:
      - text: '"p": flowers present'
        action: keypress{p ~ Y}
      - text: '"a": flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}
```

This example is now a fully valid task script that ImageAnt can run on a batch of images.

Task lists can contain any number of annotation tasks.  Let's add another task to the `main_tasks` list.
```yaml
main_tasks:
  - text: 'Does the image show a plant with flowers?'
    variable: 'flowers'
    responses:
      - text: '"p": flowers present'
        action: keypress{p ~ Y}
      - text: '"a": flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}

  - text: 'Does the image show a whole plant?'
    variable: 'whole_plant'
    responses:
      - text: '"p": whole plant present'
        action: keypress{p ~ Y}
      - text: '"a": whole plant absent'
        action: keypress{a ~ N}
```

Now, for each image, the user will be presented first with the flowers annotation task, then, after giving a response, the user will be presented with the question about whether the image shows a "whole plant".  Annotations tasks are always presented to the user in the order they are defined inside of a task list.

The output CSV file for this annotation script will include columns named `flowers` and `whole_plant`, the first of which will contain values from the set `{'Y', 'N', 'U'}`, and the second of which will contain values from the set `{'Y', 'N'}`.  All variable names defined in a task script must be unique.


## Dependent annotation task lists

For basic annotation workflows, as we've seen so far, we simply present the user with the same series of tasks for each image.  For more sophisticated annotation workflows, we might want to choose which tasks we present to a user depending on the user's responses to earlier tasks.

For example, in our sample annotation task script, we ask the user to indicate whether an image shows a plant with flowers.  Suppose that, for images that have flowers, we'd like to gather some additional information, such as whether or not the flowers are open (flower buds are considered to be flowers that have not yet opened).  If the image does not show a plant with flowers, there is no reason to show the user any additional annotation tasks related to flowers.  To do this, we can define a new task list containing our flower-specific annotation tasks.  Then, we use the `jump-to` command to indicate that if a user annotates an image as showing a plant with flowers, ImageAnt should run the flower-specific annotation tasks.  When we put all of these pieces together, the script looks like this:
```yaml
main_tasks:
  - text: 'Does the image show a plant with flowers?'
    variable: 'flowers'
    responses:
      - text: '"p": flowers present'
        action: keypress{p ~ Y}
        jump-to: flower_tasks
      - text: '"a": flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}

  - text: 'Does the image show a whole plant?'
    variable: 'whole_plant'
    responses:
      - text: '"p": whole plant present'
        action: keypress{p ~ Y}
      - text: '"a": whole plant absent'
        action: keypress{a ~ N}

#
# Only run these tasks if the image shows a plant with flowers.
#
flower_tasks:
  - text: 'Are any of the flowers open?'
    variable: 'open_flowers'
    responses:
      - text: '"p": open flowers present'
        action: keypress{p ~ Y}
      - text: '"a": open flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}
```

Notice the `jump-to` command in the first response of the first task in `main_tasks`.  Note also that, as with any task list, `flower_tasks` could contain more than one task.

The `jump-to` command indicates that the tasks in `flower_tasks` are dependent on the first annotation task in `main_tasks` and that `flower_tasks` will only be run if the user indicates that an image shows a plant with flowers.  Once all annotation tasks in `flower_tasks` are completed, ImageAnt will continue running the remaining annotation tasks in the `main_tasks` list.  With the addition of the new annotation task, the output CSV file will now include 3 columns, called `flowers`, `whole_plant`, and `open_flowers`, one for each annotation task.

This raises a question: If a user indicates that an image _does not_ show a plant with flowers, and the user is therefore not shown any tasks in `flower_tasks`, what will be the value of the variable `open_flowers` for that image?  The answer is that, by default, the value of the variable `open_flowers` will be set to an empty string (`''`) if the task is not presented to the user.  To summarize:
  * If the value of `flowers` for a given image is `'N'` (i.e., the user indicates the image _does not_ show a plant with flowers), then the value of `open_flowers` will also be an empty string (`''`).
  * If the value of `flowers` is `'U'` (i.e., the user indicates it is impossible to determine whether the image shows a plant with flowers), the value of `open_flowers` will again be an empty string (`''`).
  * And, if the value of `flowers` is `'Y'` (i.e., the user indicates the image _does_ show a plant with flowers), then the value of `open_flowers` will be whatever the user chooses in response to the `open_flowers` annotation task.

We can make this behavior explicit by adding a `default` directive to the `open_flowers` annotation task:
```yaml
main_tasks:
  - text: 'Does the image show a plant with flowers?'
    variable: 'flowers'
    responses:
      - text: '"p": flowers present'
        action: keypress{p ~ Y}
        jump-to: flower_tasks
      - text: '"a": flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}

  - text: 'Does the image show a whole plant?'
    variable: 'whole_plant'
    responses:
      - text: '"p": whole plant present'
        action: keypress{p ~ Y}
      - text: '"a": whole plant absent'
        action: keypress{a ~ N}

#
# Only run these tasks if the image shows a plant with flowers.
#
flower_tasks:
  - text: 'Are any of the flowers open?'
    variable: 'open_flowers'
    default: ''
    responses:
      - text: '"p": open flowers present'
        action: keypress{p ~ Y}
      - text: '"a": open flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}
```

By giving the empty string as the default value for the `open_flowers` task, we indicate that if the `open_flowers` task is not presented to the user, an empty string should be recorded as the value of the `open_flowers` variable.  Again, this is ImageAnt's default behavior and it would happen automatically if no `default` directive were provided.  You can provide any value you like as the default.  If, in the example above, we instead said `default: 'empty'`, then the string `'empty'` would be used as the value of the variable `open_flowers` whenever the task was not presented to the user.

In many cases, it is useful to propagate annotation values from a parent task to all of its dependent tasks.  For instance, if an image does not show a plant with flowers, we know that it cannot show any open flowers, and so we might like to automatically record an `'N'` for both `flowers` and `open_flowers`.

To implement this behavior, we can provide dependent tasks with a `default` directive with the special value `inherit`:
```yaml
main_tasks:
  - text: 'Does the image show a plant with flowers?'
    variable: 'flowers'
    responses:
      - text: '"p": flowers present'
        action: keypress{p ~ Y}
        jump-to: flower_tasks
      - text: '"a": flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}

  - text: 'Does the image show a whole plant?'
    variable: 'whole_plant'
    responses:
      - text: '"p": whole plant present'
        action: keypress{p ~ Y}
      - text: '"a": whole plant absent'
        action: keypress{a ~ N}

#
# Only run these tasks if the image shows a plant with flowers.
#
flower_tasks:
  - text: 'Are any of the flowers open?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: '"p": open flowers present'
        action: keypress{p ~ Y}
      - text: '"a": open flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}
```
With a value of `inherit`, the `default` directive indicates a dependent task should inherit annotation values from its parent task whenever the dependent task is not presented to the user.  In our example, this means that:
  * If the value of `flowers` for a given image is `'N'` (i.e., the user indicates the image _does not_ show a plant with flowers), then the value of `open_flowers` will also be `'N'`.
  * If the value of `flowers` is `'U'` (i.e., the user indicates it is impossible to determine whether the image shows a plant with flowers), the value of `open_flowers` will also be `'U'`.
  * And, if the value of `flowers` is `'Y'` (i.e., the user indicates the image _does_ show a plant with flowers), then the value of `open_flowers` will be whatever the user chooses in response to the `open_flowers` annotation task.

Finally, note that chains of dependent task lists can be as long as you'd like.  For instance, we could define another task list, designed to obtain more information about open flowers (e.g., are anthers visible?), and we could add a `jump-to` directive to the response for `open flowers present`.  The resulting task script might look like this:
```yaml
main_tasks:
  - text: 'Does the image show a plant with flowers?'
    variable: 'flowers'
    responses:
      - text: '"p": flowers present'
        action: keypress{p ~ Y}
        jump-to: flower_tasks
      - text: '"a": flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}

  - text: 'Does the image show a whole plant?'
    variable: 'whole_plant'
    responses:
      - text: '"p": whole plant present'
        action: keypress{p ~ Y}
      - text: '"a": whole plant absent'
        action: keypress{a ~ N}

#
# Only run these tasks if the image shows a plant with flowers.
#
flower_tasks:
  - text: 'Are any of the flowers open?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: '"p": open flowers present'
        action: keypress{p ~ Y}
        jump-to: open_flower_tasks
      - text: '"a": open flowers absent'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}

#
# Only run these tasks if the image shows a plant with open flowers.
#
open_flower_tasks:
  - text: 'Are anthers visible?'
    variable: 'anthers_visible'
    default: inherit
    responses:
      - text: '"p": anthers visible'
        action: keypress{p ~ Y}
      - text: '"a": anthers not visible'
        action: keypress{a ~ N}
      - text: '"u": uncertain'
        action: keypress{u ~ U}
```

Notice that the `default` values for both dependent tasks are set to `inherit`.  That means that if a user answers the initial `flowers` task by indicating that flowers are not present, the user will not see either the `open_flowers` task or the `anthers_visible` task.  The value of the `flowers` variable will be `'N'`, the variable `open_flowers` will inherit this value and also be set to `'N'`, and the variable `anthers_visible` will inherit the value of `open_flowers` and so will also be set to `'N'`.

As you can see, with a relatively simple language for defining image annotation tasks, ImageAnt makes it possible to define complex annotation scripts with multiple dependency chains.  That, in turn, makes it possible to efficiently generate complex annotation data.

