#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Tests five methods for calculating waveform shape.
#
# The results with CPython 3.6.9 on GNU/Linux, using an 18-second, 44.1 kHz
# test .wav file (dtmf_tones.wav) (best time in seconds out of 20 trials):
#   python: 0.427
#   pythonnumpy: 0.00474
#   numpyapprox: 0.000498
#   numpy: 0.921
#   python, list input: 2.712
#

from package_context import imageant

import sys
import timeit
import librosa
from PyQt5.QtWidgets import QApplication
from imageant.gui.audio_widgets import WaveformView


app = QApplication(sys.argv)

a_data, samp_rate = librosa.core.load('dtmf_tones.wav', sr=None, mono=True)
wfv = WaveformView()
wfv.setAudioData(a_data)


def _python():
    wfv._calcWaveShape_Py(602)

def _pythonnumpy():
    wfv._calcWaveShape_PyNumpy(602)

def _numpyapprox():
    wfv._calcWaveShape_NumpyApprox(602)

def _numpy():
    wfv._calcWaveShape_Numpy(602)


reps = 20
#_python()
#exit()

res = timeit.repeat(_python, repeat=reps, number=1)
print('python:', min(res))
res = timeit.repeat(_pythonnumpy, repeat=reps, number=1)
print('pythonnumpy:', min(res))
res = timeit.repeat(_numpyapprox, repeat=reps, number=1)
print('numpyapprox:', min(res))
res = timeit.repeat(_numpy, repeat=reps, number=1)
print('numpy:', min(res))

wfv.setAudioData(a_data.tolist())

res = timeit.repeat(_python, repeat=reps, number=1)
print('python:', min(res))

