#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Tests five methods for creating a low-level array of unsigned chars.
# Values are assigned three at a time to simulate generating RGB image data.
#
# The results with CPython 3.6.9 on GNU/Linux (best time in seconds out of 20
# trials):
#   array, preallocated: 2.86
#   array, extend: 2.81
#   bytearray, preallocated: 2.31
#   bytearray, preallocated, slice assignment: 3.58
#   bytearray, extend: 1.98
# 

import array
import timeit


def array_preallocate():
    a = array.array('B', bytes(pxcnt * 3))
    for i in range(pxcnt):
        a[i*3] = 255
        a[i*3+1] = 255
        a[i*3+2] = 255

def array_extend():
    a = array.array('B')
    for i in range(pxcnt):
        a.extend((255, 255, 255))

def bytearray_preallocate():
    a = bytearray(pxcnt * 3)
    for i in range(pxcnt):
        a[i*3] = 255
        a[i*3+1] = 255
        a[i*3+2] = 255

def bytearray_preallocate_slice():
    a = bytearray(pxcnt * 3)
    for i in range(pxcnt):
        a[i*3:i*3+3] = (255, 255, 255)

def bytearray_extend():
    a = bytearray()
    for i in range(pxcnt):
        a.extend((255, 255, 255))


pxcnt = 10000000
reps = 20

res = timeit.repeat(array_preallocate, repeat=reps, number=1)
print('array, preallocated:', min(res))

res = timeit.repeat(array_extend, repeat=reps, number=1)
print('array, extend:', min(res))

res = timeit.repeat(bytearray_preallocate, repeat=reps, number=1)
print('bytearray, preallocated:', min(res))

res = timeit.repeat(bytearray_preallocate_slice, repeat=reps, number=1)
print('bytearray, preallocated, slice assignment:', min(res))

res = timeit.repeat(bytearray_extend, repeat=reps, number=1)
print('bytearray, extend:', min(res))

