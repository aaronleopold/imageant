#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Tests four methods for creating a spectrogram.
#
# The results with CPython 3.6.9 on GNU/Linux, using an 18-second, 44.1 kHz
# test .wav file (dtmf_tones.wav), 1024 window size, and 50% widnow overlap
# (best time in seconds out of 20 trials):
#   numpy/custom: 0.435
#   matplotlib: 0.742
#   librosa: 0.741
#   numpy_strided/custom: 0.338
#

from package_context import imageant

import timeit
import librosa
from imageant.audio.spectrogram import Spectrogram


a_data, samp_rate = librosa.core.load('dtmf_tones.wav', sr=None, mono=True)
spec = Spectrogram()
spec.setInputData(a_data)

stride = spec.getWindowSize() - int(spec.getWindowSize() * spec.getOverlap())

def _numpy():
    img = spec._getSpecData_Numpy(a_data, stride)

def _numpy_strided():
    img = spec._getSpecData_NumpyStrided(a_data, stride)

def _matplotlib():
    img = spec._getSpecData_Matplotlib(a_data, stride)

def _librosa():
    img = spec._getSpecData_Matplotlib(a_data, stride)


reps = 20
#img = spec._getSpecData_Numpy(a_data, stride)
#print(img)
#print(img.shape)
#img = spec._getSpecData_Librosa(a_data, stride)
#print(img)
#print(img.shape)
#img = spec._getSpecData_NumpyStrided(a_data, stride)
#print(img)
#print(img.shape)
#img = spec._getSpecData_Matplotlib(a_data, stride)
#print(img)
#print(img.shape)

res = timeit.repeat(_numpy, repeat=reps, number=10)
print('numpy/custom:', min(res))
res = timeit.repeat(_matplotlib, repeat=reps, number=10)
print('matplotlib:', min(res))
res = timeit.repeat(_librosa, repeat=reps, number=10)
print('librosa:', min(res))
res = timeit.repeat(_numpy_strided, repeat=reps, number=10)
print('numpy_strided/custom:', min(res))

