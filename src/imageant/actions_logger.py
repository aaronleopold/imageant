# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import os.path
import json


class AnnotationActionsLogger:
    """
    Efficient logging and recovery of media object annotation actions.
    """
    # Annotation actions.
    A_SKIP = 0  # Indicates a media object was skipped.
    A_RESET = 1 # Indicates that all annotations were reset for a media object.
    A_TIME = 2  # Associates a time interval with a media object.

    def __init__(self, log_file, mode='w'):
        """
        The mode values should follow Python conventions for opening files.  If
        the mode is 'r', the logger will be read-only and usable for replaying
        logged actions.
        """
        if mode not in ('r', 'w', 'a'):
            raise Exception('Invalid logger mode: "{0}".'.format(mode))

        if mode == 'r':
            self.logfile = open(log_file, 'r')
        else:
            # Use line output buffering for log writes so that the output
            # buffer is automatically flushed after each log event.
            self.logfile = open(log_file, mode, buffering=1)

        # The order in which to write output variable valuables.  By using a
        # constant output order, we can avoid writing variable names more than
        # once and thus make the log files more compact.
        self.varname_order = None

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self):
        if self.logfile is not None:
            self.logfile.close()

        self.logfile = None

    def getLogFile(self):
        """
        Returns the path of the log file or None if none is set.
        """
        if self.logfile is not None:
            return os.path.abspath(self.logfile.name)
        else:
            return None

    def _logEncode(self, obj_id, action, time):
        """
        Encodes a media object annotation action and time into a log file
        string.

        obj_id (str): The identifier of the media object.
        action: The annotation action; either an action constant or a
            dictionary of annotation values.
        time (float-like): The time value associated with the annotation
            action.
        """
        # For representational consistency, ensure that all times are encoded
        # as floating-point numbers.
        time = float(time)

        if action in (self.A_SKIP, self.A_RESET, self.A_TIME):
            payload = action
        elif isinstance(action, dict):
            # The action is a set of annotation variable values, so convert
            # them to a values list.
            if self.varname_order is None:
                raise Exception('Missing variable name ordering.')

            payload = []
            for varname in self.varname_order:
                if varname in action:
                    payload.append(action[varname])
                else:
                    raise Exception(
                        'The variable name "{0}" does not match the expected '
                        'variable names in the annotation actions '
                        'log.'.format(varname)
                    )
        else:
            raise Exception('Unrecognized annotation action.')

        # Note that JSON makes a good log encoding because it guarantees that
        # any newlines in data strings will be escaped, which ensures that each
        # line in a log file corresponds with one log entry.
        logstr = json.dumps((obj_id, payload, time), separators=(',', ':'))
        logstr = logstr[1:-1]

        return logstr

    def _logDecode(self, logstr):
        """
        Decodes a media object annotation action and returns the result as the
        tuple (media_object_id, annotation_action, time).
        """
        logstr = '[' + logstr + ']'
        obj_id, payload, time = json.loads(logstr)

        if payload in (self.A_SKIP, self.A_RESET, self.A_TIME):
            action = payload
        elif isinstance(payload, list):
            # The action is a list of annotation variable values which need to
            # be converted to a dictionary.
            if self.varname_order is None:
                raise Exception('Missing variable name ordering.')

            action = {}
            for cnt, varname in enumerate(self.varname_order):
                action[varname] = payload[cnt]
        else:
            raise Exception('Unrecognized annotation action.')

        return (obj_id, action, time)

    def logAction(self, media_obj_id, action, time):
        """
        Logs a single annotation action for a media object and the time
        interval associated with the action.
        """
        if self.logfile is None:
            raise Exception('Invalid operation on a closed log file.')

        if isinstance(action, dict) and self.varname_order is None:
            self.varname_order = sorted(action.keys())
            self.logfile.write(
                json.dumps(self.varname_order, separators=(',', ':')) + '\n'
            )

        self.logfile.write(
            self._logEncode(media_obj_id, action, time) + '\n'
        )

    def readAction(self):
        """
        Reads a single annotation action from the log and returns it as the
        tuple (media_object_id, annotation_action, time).  If no actions remain
        in the log, returns None.
        """
        if self.logfile is None:
            raise Exception('Invalid operation on a closed log file.')

        logstr = self.logfile.readline()
        if logstr == '':
            # '' signals EOF.
            return None

        logstr = logstr.strip()

        if logstr.startswith('['):
            # The line should be a variable order list.
            varname_order = json.loads(logstr)

            if (
                self.varname_order is not None and
                varname_order != self.varname_order
            ):
                raise Exception(
                    'Invalid attempt to redefine variable order in log file.'
                )

            self.varname_order = varname_order

            return self.readAction()
        else:
            return self._logDecode(logstr)

