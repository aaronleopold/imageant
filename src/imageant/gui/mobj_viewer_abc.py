# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class MediaObjectViewer:
    """
    Defines the minimal interface for all media type-specific viewers.  This
    would seem to be a good candidate for implementation using Python's ABC
    mechanism, but that would introduce metaclass conflicts when attempting
    multiple inheritance with Qt interface classes.  Rather than dealing with
    custom metaclasses, it is much easier to just take a less formal approach
    to interface definition.
    """
    # Define the zoom modes.
    ZOOM_CUSTOM = 0
    ZOOM_FIT = 1

    def getKeyboardShortcutsInfo(self):
        """
        Returns information about the keyboard shortcuts implemented by this
        media object viewer.  The help information should be returned in the
        following format:
        (
            # Shortcuts group 1.
            (
                ('SHORTCUT KEYS', 'DESCRIPTION'),
                ('SHORTCUT KEYS', 'DESCRIPTION')
            ),
            # Shortcuts group 2.
            (
                ('SHORTCUT KEYS', 'DESCRIPTION'),
                ('SHORTCUT KEYS', 'DESCRIPTION')
            )
        )
        """
        raise NotImplementedError()

    def loadMediaObject(self, mfile_path):
        """
        Load and display a new media file.
        """
        raise NotImplementedError()

    def unloadMediaObject(self):
        """
        Unload the currently displayed media object, if any.
        """
        raise NotImplementedError()

    def getMediaObjTime(self):
        """
        Returns the total time in seconds that the current media object has
        been displayed.
        """
        raise NotImplementedError()

    def zoomable(self):
        """
        Returns True if this media object viewer supports zoom functionality.
        """
        raise NotImplementedError()

    def zoom(self, zoom_mult):
        """
        Zooms the media object view by a given size multiplier.
        """
        raise NotImplementedError()

    def zoomFullSize(self):
        """
        Zoom the media object view to the maximum possible size.
        """
        raise NotImplementedError()

    def zoomToFit(self):
        """
        Zooms the media object view to fit optimally in the viewer's screen
        space.
        """
        raise NotImplementedError()

