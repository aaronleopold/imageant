# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import QSize, QRect
from PyQt5.QtGui import QPalette, QPainter, QColor
import numpy as np
from ..audio.spectrogram import Spectrogram


class WaveformView(QWidget):
    def __init__(self):
        super().__init__()

        self.setBackgroundRole(QPalette.Base)
        self.setAutoFillBackground(True)

        self.adata = None

        self.waveshape = [[0,0]]
        self.waveshape_len = 0

        # A "dirty bit" to track whether the waveshape data are out of sync
        # with the current audio data (e.g., when the audio file changes).
        self.waveshape_dirty = False

        # The absolute amplitude threshold that triggers auto-amplification to
        # ensure that low-amplitude audio signals are still visible.
        self.amp_threshold = 0.5

    def sizeHint(self):
        return QSize(600, 200)

    def minimumSizeHint(self):
        return QSize(100, 40)

    def setAudioData(self, audio_data):
        """
        audio_data: A 1-D numpy array of floating-point audio samples with
        values in the range [-1.0, 1.0].  No DC offset should be present (i.e.,
        the mean of the samples should be 0).
        """
        self.adata = audio_data
        self.waveshape_dirty = True
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        #painter.setRenderHint(QPainter.Antialiasing, True)

        if self.adata is not None:
            self.drawWave(painter)

        #painter.drawLine(0, 0, self.width(), 0)

    def _calcWaveShape_Py(self, width):
        """
        Calculates the outline of the waveform for display at the given width.
        This method implements an exact calculation that precisely maps each
        sample to the correct pixel.
        """
        sampcnt = len(self.adata)

        if len(self.waveshape) < width:
            self.waveshape = [[0,0] for i in range(width)]

        # Iterate through each sample, calculate its corresponding x-pixel
        # value, and save the min/max at each x-pixel value.
        for i in range(sampcnt):
            x = (i * width) // sampcnt
            if self.adata[i] < self.waveshape[x][0]:
                self.waveshape[x][0] = self.adata[i]
            elif self.adata[i] > self.waveshape[x][1]:
                self.waveshape[x][1] = self.adata[i]

        #print(self.waveshape[width-10:width])
        self.waveshape_len = width

    def _calcWaveShape_PyNumpy(self, width):
        """
        Calculates the outline of the waveform for display at the given width.
        This method implements an exact calculation that precisely maps each
        sample to the correct pixel.  Due to offloading many of the
        calculations to numpy, it is much more efficient than the pure python
        version.  The algorithm is also much less straightforward.
        """
        sampcnt = len(self.adata)

        if len(self.waveshape) < width:
            self.waveshape = [[0,0] for i in range(width)]

        # Iterate through the x-pixel values and calculate the index of the
        # first sample that maps to each x-pixel value.
        prev_i = 0
        for x in range(1, width + 1):
            # Efficiently calculate the inverse of the sample -> x-pixel value
            # mapping.  The correctness of this implementation is not at all
            # obvious.  The sample -> x-pixel value mapping is
            # x = floor((width * i) / sampcnt).  By rewriting the floor
            # function as an inequality statement, it is possible to derive the
            # inverse (for the first sample in the mapping) as
            # i = ceil((sampcnt * x) / width).  Then, using the equality
            # ceil(n/m) = floor((n - 1)/m) + 1, we arrive at
            # i = floor((sampcnt * x - 1) / width) + 1.
            i = ((x * sampcnt - 1) // width) + 1
            #print(i)
            subsamp = self.adata[prev_i:i]
            self.waveshape[x-1][0] = np.min(subsamp)
            self.waveshape[x-1][1] = np.max(subsamp)
            prev_i = i

        #print(self.waveshape[width-10:width])
        #print(self.waveshape[:10])
        self.waveshape_len = width

    def _calcWaveShape_NumpyApprox(self, width):
        """
        Calculates the outline of the waveform for display at the given width.
        This method implements an approximation based on reshaping and
        analyzing the samples in numpy, so it does not precisely map each
        sample to the correct pixel.  The accuracy depends on how near the
        display width is to being a multiple of the number of samples.  The
        time synchronization at the end of the signal is guaranteed to be off
        by no more than (width - 1) number of samples.  E.g., with a display
        width of 1,920 px, the maximum possible time synchronization error at
        the end of the signal is ~0.044 s.  In practice, the approximation
        appears to work well and this method is about 3 orders of magnitude
        faster than the pure python implementation.
        """
        sampcnt = len(self.adata)

        # Calculate the number of samples to aggregate in each pixel.  We'll
        # analyze any leftover samples as part of the last pixel aggregate.
        px_agg = sampcnt // width
        remainder = sampcnt % width

        view = np.reshape(self.adata[:width*px_agg], (width, px_agg))
        #print(view)
        #print('sampcnt:', sampcnt)
        #print(view.shape, ':', (view.shape[0] * view.shape[1]))

        mins = np.min(view, axis=1)
        maxs = np.max(view, axis=1)

        # Analyze any leftover samples.
        if remainder > 0:
            rem_min = np.min(self.adata[width*px_agg:])
            rem_max = np.max(self.adata[width*px_agg:])
            if rem_min < mins[-1]:
                mins[-1] = rem_min
            if rem_max < maxs[-1]:
                maxs[-1] = rem_max

        #print(mins)
        #print(len(mins))

        self.waveshape = np.column_stack((mins, maxs))
        #print(self.waveshape[width-10:width])
        self.waveshape_len = width

    def _calcWaveShape_Numpy(self, width):
        """
        Calculates the outline of the waveform for display at the given width.
        This is relatively simple implementation based entirely on numpy
        operations, but it is slow, presumably due to the repeated boolean
        indexing operations.
        """
        sampcnt = len(self.adata)

        # Construct a vector that maps sample indices to display indices.
        imap = (np.arange(sampcnt) * width) // sampcnt

        mins = [self.adata[imap==i].min() for i in range(width)]
        maxs = [self.adata[imap==i].max() for i in range(width)]
        #print(mins)

    def drawWave(self, p):
        sampcnt = len(self.adata)
        if sampcnt < 2:
            return

        p.save()

        p.translate(0, self.height() / 2)

        width = self.width()
        hheight = (self.height() / 2) * -1

        # Draw the baseline (x axis).
        p.setPen(QColor(86, 86, 86))
        p.drawLine(0, 0, width, 0)

        p.setPen(QColor(0, 0, 180))

        # I tested the performance of direct drawing versus pre-computing the
        # wave shape on a 19-second, 44.1 kHz .wav file, using the pure python
        # implementation of wave shape calculation.  With direct drawing, the
        # best drawing time out of 7 trials was 4.450 s.  With pre-computing
        # the wave shape, the best performance out of 7 trials was 0.464 s.
        if sampcnt > (width * 4):
            # Recalculate the wave shape, if needed.
            if self.waveshape_len != width or self.waveshape_dirty:
                self._calcWaveShape_PyNumpy(width)
                self.waveshape_dirty = False

            # Calculate a scaling factor, if needed, so that very low-amplitude
            # audio signals will still be visible.
            samp_max = max([samp[1] for samp in self.waveshape])
            samp_min = min([samp[0] for samp in self.waveshape])
            if abs(samp_min) > samp_max:
                samp_max = abs(samp_min)
            if samp_max < self.amp_threshold:
                scale_fact = self.amp_threshold / abs(samp_min) * hheight
            else:
                scale_fact = hheight

            # Draw the first segment of the wave shape.
            p.drawLine(
                0, self.waveshape[0][0] * scale_fact,
                0, self.waveshape[0][1] * scale_fact
            )
            # Draw the remaining waveshape segments.
            for x in range(1, self.waveshape_len):
                #print(self.waveshape[x])
                p.drawLine(
                    x, self.waveshape[x][0] * scale_fact,
                    x, self.waveshape[x][1] * scale_fact
                )

                # For relatively small numbers of samples, ensure that
                # consecutive line segments are visibly connected to preserve
                # waveform continuity.
                if self.waveshape[x][1] < self.waveshape[x-1][0]:
                    p.drawLine(
                        x-1, self.waveshape[x-1][0] * scale_fact,
                        x, self.waveshape[x][1] * scale_fact
                    )
                elif self.waveshape[x][0] > self.waveshape[x-1][1]:
                    p.drawLine(
                        x-1, self.waveshape[x-1][1] * scale_fact,
                        x, self.waveshape[x][0] * scale_fact
                    )

        # For small numbers of audio samples, just draw each sample directly.
        else:
            # Calculate a scaling factor, if needed, so that very low-amplitude
            # audio signals will still be visible.
            samp_max = np.max(self.adata)
            samp_min = np.min(self.adata)
            if abs(samp_min) > samp_max:
                samp_max = abs(samp_min)
            if samp_max < self.amp_threshold:
                y_scale_fact = self.amp_threshold / abs(samp_min) * hheight
            else:
                y_scale_fact = hheight

            x_scale_fact = (width - 1) / (sampcnt - 1)

            # Draw the audio signal.
            for i in range(1, len(self.adata)):
                p.drawLine(
                    (i-1) * x_scale_fact, self.adata[i-1] * y_scale_fact,
                    i * x_scale_fact, self.adata[i] * y_scale_fact
                )

        p.restore()


class SpectrogramView(QWidget):
    def __init__(self):
        super().__init__()

        self.setBackgroundRole(QPalette.Base)
        self.setAutoFillBackground(True)

        self.adata = None
        self.specdata = None

        self.spec = Spectrogram()
        self.specbuff = None

    def sizeHint(self):
        return QSize(600, 200)

    def minimumSizeHint(self):
        return QSize(100, 40)

    def setAudioData(self, audio_data):
        """
        audio_data: A 1-D numpy array of floating-point audio samples with
        values in the range [-1.0, 1.0].  No DC offset should be present (i.e.,
        the mean of the samples should be 0).
        """
        self.adata = audio_data

        if self.adata is not None:
            self.spec.setInputData(audio_data)

            #stime = time.perf_counter()
            self.specbuff = self.spec.getImage()
            #print(time.perf_counter() - stime)
        else:
            self.specbuff = None

        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.SmoothPixmapTransform, True)

        if self.specbuff is not None:
            painter.drawImage(
                QRect(0, 0, self.width(), self.height()),
                self.specbuff,
                QRect(0, 0, self.specbuff.width(), self.specbuff.height())
            )

