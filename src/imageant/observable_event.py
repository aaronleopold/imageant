# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class ObservableEventError(Exception):
    pass


class ObservableEvent:
    """
    A class that implements an observable event type for easy implementation of
    the "observable" pattern.  Instances of this class can be used as "event
    types" that can register observers, and event type instances can be called
    like functions or class methods to trigger notification of their observers.
    The intended usage is very similar to the way "signals" and "slots" are
    implemented in the Qt framework.
    """
    def __init__(self):
        self._observers = set()

    def __call__(self, *args):
        """
        Triggers observer notifications for this event.  All values in *args
        will be passed on as arguments to the observers.

        args: Arguments to send to the observers.
        """
        for observer in self._observers:
            observer(*args)

    def registerObserver(self, observer):
        """
        Registers a new observer that will be notified whenever this event is
        triggered.

        observer: A callable object (typically a function or method) with an
            interface that matches the arguments passed when the event occurs.
        """
        self._observers.add(observer)

    def unregisterObserver(self, observer):
        """
        Removes an observer from the set of observers that will be notified
        when this event is triggered.
        """
        try:
            self._observers.remove(observer)
        except KeyError:
            raise ObservableEventError(
                'The callable is not an observer of this event.'
            )

