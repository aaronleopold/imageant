# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import strictyaml
from strictyaml import Map, MapPattern, Str, Seq, Optional, YAMLValidationError
from ruamel.yaml import YAML as RuamelYAML
from ruamel.yaml.error import MarkedYAMLError
from .task import AnnotationTask, TaskResponse
from .task_script import TaskScript


class TaskScriptParseError(Exception):
    """
    An exception class for reporting errors encountered when parsing task
    scripts.
    """
    pass


# Define a basic schema for preliminary parsing of the input.  This ony does
# basic type checking, so additional, custom syntax and consistency checking is
# required for the various components.
_yaml_schema = MapPattern(
    Str(), Seq(
        Map({
            'text': Str(),
            'variable': Str(),
            Optional('default'): Str(),
            'responses': Seq(
                Map({
                    'action': Str(),
                    Optional('text'): Str(),
                    Optional('jump-to'): Str()
                })
            )
        })
    )
)


class TaskScriptParser:
    def __init__(self):
        pass

    def _getNextActionStrToken(self, action_str):
        """
        Returns the next token when parsing an action specification string and
        the remainder of the action string as the tuple (token, remainder).
        """
        SINGLE_CHAR_TOKS = ('{', '}', '~')

        action_str = action_str.lstrip()

        if len(action_str) == 0:
            return ('', '')

        if action_str[0] in SINGLE_CHAR_TOKS:
            return (action_str[0], action_str[1:])
        else:
            # The code for parsing string tokens is somewhat complex because we
            # need to support quoted and unquoted string productions, either of
            # which can include whitespace inside a string, and the token stop
            # rules are different for quoted and unquoted strings.  See the
            # unit test for examples.
            token = ''

            if len(action_str) > 1 and action_str[0] in ("'", '"'):
                # Parsing a quoted string.
                quote_chr = action_str[0]
                in_quote = True

                cnt = 1
                while cnt < len(action_str) and in_quote:
                    if action_str[cnt] == quote_chr:
                        in_quote = False
                    else:
                        token += action_str[cnt]

                    cnt += 1
            else:
                # Parsing an unquoted string.
                cnt = 0
                while (
                    cnt < len(action_str) and
                    action_str[cnt] not in SINGLE_CHAR_TOKS
                ):
                    token += action_str[cnt]

                    cnt += 1

                token = token.strip()

            return (token, action_str[cnt:])

    def _parseActionStr(self, action_str):
        """
        Parses a task response action specification string.  Returns the
        results as the tuple (ACTION_TYPE, key_val, response_val).  Unless
        ACTION_TYPE is TaskResponse.KEYPRESS, key_val and response_val will be
        None. 
        """
        astr = action_str.strip()

        token, astr = self._getNextActionStrToken(astr)
        if token == 'keypress':
            token, astr = self._getNextActionStrToken(astr)
            if token != '{':
                raise TaskScriptParseError(
                    'Invalid task response action specification: "{0}".  The '
                    'key mapping must be enclosed in braces '
                    '("{{}}").'.format(action_str)
                )

            token, astr = self._getNextActionStrToken(astr)
            if len(token) != 1:
                raise TaskScriptParseError(
                    'Invalid key ("{0}") for a key press response in the '
                    'action specification "{1}".'.format(token, action_str)
                )
            key_val = token

            token, astr = self._getNextActionStrToken(astr)
            if token != '~':
                raise TaskScriptParseError(
                    'Unexpected character(s) ("{0}") in the task response '
                    'action specification "{1}".'.format(token, action_str)
                )

            token, astr = self._getNextActionStrToken(astr)
            response_val = token

            token, astr = self._getNextActionStrToken(astr)
            if token != '}':
                raise TaskScriptParseError(
                    'Invalid task response action specification: "{0}".  The '
                    'key mapping must be enclosed in braces '
                    '("{{}}").'.format(action_str)
                )

            if len(astr) > 0:
                raise TaskScriptParseError(
                    'Trailing characters after a response action '
                    'specification: "{0}".'.format(astr)
                )

            return (TaskResponse.KEYPRESS, key_val, response_val)
        else:
            raise TaskScriptParseError(
                'Invalid task response action specification: "{0}".  Unknown '
                'response type: "{1}".'.format(action_str, token)
            )

    def parseFromFile(self, script_path):
        with open(script_path) as fin:
            contents = fin.read()

        return self.parseFromStr(contents)

    def parseFromStr(self, script_str):
        script = TaskScript()

        # StrictYAML does not correctly parse documents that contain only
        # comments or comments with a single scalar value, so we use
        # ruamel.yaml directly to check for empty scripts.
        ryaml = RuamelYAML()
        rawdata = ryaml.load(script_str)
        if rawdata is None:
            return script

        rawdata = (strictyaml.load(script_str, _yaml_schema))

        # Create all of the task lists so we know which list names are valid.
        for listname in rawdata:
            script.addTaskList(str(listname))

        # A set to verify that all variable names are unique.
        t_var_names = set()

        for listname in rawdata:
            tlist = script[str(listname)]

            # Parse each annotation task definition.
            for taskdata in rawdata[listname]:
                t_text = str(taskdata['text'])
                t_var = str(taskdata['variable'])
                t_default = None
                if 'default' in taskdata:
                    t_default = str(taskdata['default'])

                if t_var in t_var_names:
                    raise TaskScriptParseError(
                        'The variable name "{0}" was used for more than one '
                        'annotation task.  All annotation task variable names '
                        'must be unique.'.format(t_var)
                    )
                else:
                    t_var_names.add(t_var)

                task = AnnotationTask(t_text, t_var, t_default)
                tlist.append(task)

                # Parse each response definition within the task.
                for r_data in taskdata['responses']:
                    r_text = ''
                    if 'text' in r_data:
                        r_text = str(r_data['text'])

                    jt_key = None
                    if 'jump-to' in r_data:
                        jt_key = str(r_data['jump-to'])
                        if jt_key not in script:
                            raise TaskScriptParseError(
                                'The jump-to target "{0}" is not a valid '
                                'annotation task list name.'.format(jt_key)
                            )

                    action_tuple = self._parseActionStr(str(r_data['action']))
                    r_type, r_key_val, r_response_val = action_tuple

                    task.addResponse(
                        r_type, r_text, jt_key, r_key_val, r_response_val
                    )

        return script

