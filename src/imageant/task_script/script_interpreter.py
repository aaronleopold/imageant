# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .task import AnnotationTask, TaskResponse
from .task_script import TaskScript


class TaskScriptError(Exception):
    """
    An exception class for reporting run-time errors for task scripts.
    """
    pass


class _StateMemento:
    """
    A "private" class for capturing and restoring internal interpreter state.
    """
    def __init__(
        self, cur_list, cur_task, tlist_stack, lname_stack, response_vals
    ):
        """
        cur_list: The name of the current task list.
        cur_task: The index of the current task.
        tlist_stack: Run-time stack for task lists.
        lname_stack: Task list stack for dependency cycle detection.
        response_vals: The response variable values recorded so far.
        """
        self._cur_list = cur_list
        self._cur_task = cur_task

        # Shallow copies of the stacks and response dictionary are sufficient
        # here because all contained objects will either be strings or tuples
        # of strings (i.e., all contained objects are immutable).
        self._tlist_stack = tlist_stack.copy()
        self._lname_stack = lname_stack.copy()
        self._response_vals = response_vals.copy()


class TaskScriptInterpreter:
    def __init__(self, task_script):
        """
        Initializes the interpreter with a task script and begins executing the
        task script.
        """
        self.script = task_script

        # A list to serve as a stack for saving state when recursing
        # through task list dependencies.
        self._tlist_stack = []

        # A list to serve as a stack for tracking visited task lists to
        # detect dependency cycles.  We use a separate stack here because
        # self._tlist_stack is only used for explicit recursions following
        # jump-to commands, but we need to detect cycles in both explicit
        # and implicit recursions.
        self._lname_stack = []

        self.run()

    def run(self):
        """
        Starts executing the annotation task script from the beginning.
        """
        if len(self.script) == 0:
            self._cur_list = None
        else:
            self._cur_list = self.script.getTaskListName(0)

        self._cur_task = 0
        self._response_vals = {}

        self._tlist_stack.clear()
        self._lname_stack.clear()

    def stop(self):
        """
        Stops any running script.
        """
        self._cur_list = None

        self._cur_task = 0
        self._response_vals = {}

        self._tlist_stack.clear()
        self._lname_stack.clear()

    def _collectDependentResponseVals(self, tlist_name, parent_val, rmap):
        # Check for dependency cycles.
        if tlist_name in self._lname_stack:
            stack_trace = ' -> '.join(self._lname_stack)
            stack_trace += ' -> ' + tlist_name
            raise TaskScriptError(
                'Cycle detected in dependent task lists: {0}.  Please '
                'revise the annotation task script.'.format(stack_trace)
            )

        tlist = self.script[tlist_name]

        for task in tlist:
            if task.default == 'inherit':
                rmap[task.varname] = parent_val
            else:
                rmap[task.varname] = task.default

            for dep_listname in task.dependents:
                self._lname_stack.append(tlist_name)
                self._collectDependentResponseVals(
                    dep_listname, parent_val, rmap
                )
                self._lname_stack.pop()

    def getStateMemento(self):
        """
        Returns a memento that captures the interpreter's internal state.  Note
        that the running script is *not* captured, only the state of script
        execution, which means that attempts to restore state from the returned
        memento will only be valid if the script has not changed.
        """
        return _StateMemento(
            self._cur_list, self._cur_task, self._tlist_stack,
            self._lname_stack, self._response_vals
        )

    def restoreStateFromMemento(self, memento):
        """
        Restores the internal state of the interpreter to that captured in the
        provided state memento.   Note that mementos do *not* capture the
        script that is executing, only the state of script execution, which
        means that attempts to restore state from a memento will only be valid
        if the script has not changed.
        """
        self._cur_list = memento._cur_list
        self._cur_task = memento._cur_task

        # Shallow copies of the stacks and response dictionary are sufficient
        # here because all contained objects will either be strings or tuples
        # of strings (i.e., all contained objects are immutable).
        self._tlist_stack = memento._tlist_stack.copy()
        self._lname_stack = memento._lname_stack.copy()
        self._response_vals = memento._response_vals.copy()

    def getActiveTask(self):
        """
        Returns the currently active task or None if there is no active task.
        """
        if self._cur_list is None:
            return None

        try:
            self.script[self._cur_list]
        except (IndexError, KeyError):
            return None

        if self._cur_task >= len(self.script[self._cur_list]):
            return None

        return self.script[self._cur_list][self._cur_task]

    def setResponse(self, response_id, response_str=''):
        """
        Sets the response to the current annotation task and advances the
        active annotation task.
        """
        task = self.script[self._cur_list][self._cur_task]

        if response_id < 0 or response_id >= len(task.responses):
            raise TaskScriptError(
                '{0} is an invalid response ID for the annotation task '
                '"{1}".'.format(response_id, task.varname)
            )

        resp_obj = task.responses[response_id]

        # For keypress responses, get the response value associated with the
        # response ID.
        if resp_obj.response_type == TaskResponse.KEYPRESS:
            response_str = resp_obj.response_val

        self._response_vals[task.varname] = response_str

        # Get default response values for all dependents of this task that
        # won't be explicitly run.
        for dep_listname in task.dependents:
            if dep_listname != resp_obj.jump_to:
                self._lname_stack.append(self._cur_list)
                self._collectDependentResponseVals(
                    dep_listname, response_str, self._response_vals
                )
                self._lname_stack.pop()

        # Advance to the next task.
        if resp_obj.jump_to is not None:
            # This response has a dependency, so jump to the dependent list.
            self._tlist_stack.append((self._cur_list, self._cur_task + 1))

            # Check for dependent list cycles.
            self._lname_stack.append(self._cur_list)
            if resp_obj.jump_to in self._lname_stack:
                stack_trace = ' -> '.join(self._lname_stack)
                stack_trace += ' -> ' + resp_obj.jump_to
                raise TaskScriptError(
                    'Cycle detected in dependent task lists: {0}.  Please '
                    'revise the annotation task script.'.format(stack_trace)
                )

            self._cur_list = resp_obj.jump_to
            self._cur_task = 0
        else:
            # This response does not have a dependency, so move to the next
            # task in the list.
            self._cur_task += 1
            while (
                self._cur_task >= len(self.script[self._cur_list]) and
                len(self._tlist_stack) > 0
            ):
                # We reached the end of the current task list, so check if
                # there are any task lists left in the stack to finish
                # processing.  If so, update the cycle-check list.
                self._cur_list, self._cur_task = self._tlist_stack.pop()
                self._lname_stack.pop()

    def getResponseVals(self):
        """
        Returns the values of all response variables as a dictionary.
        """
        # A shallow copy is sufficient here because the keys and values should
        # all be strings.
        return self._response_vals.copy()

