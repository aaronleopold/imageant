# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from imageant.observable_event import ObservableEvent, ObservableEventError


class ObserverStub:
    """
    A simple class that acts as an observer.
    """
    notified_cnt = 0
    received_vals = None
    def event1Fired(self, arg1, arg2):
        self.notified_cnt += 1
        self.received_vals = (arg1, arg2)


class TestObservableEvent(unittest.TestCase):
    def setUp(self):
        pass

    def test_registerObserver(self):
        event = ObservableEvent()
        observer = ObserverStub()

        event.registerObserver(observer.event1Fired)
        self.assertEqual(1, len(event._observers))

        # Verify that registering the same observer twice doesn't result in
        # duplication.
        event.registerObserver(observer.event1Fired)
        self.assertEqual(1, len(event._observers))

        observer2 = ObserverStub()
        event.registerObserver(observer2.event1Fired)
        self.assertEqual(2, len(event._observers))

    def test_unregisterObserver(self):
        event = ObservableEvent()

        observer = ObserverStub()
        observer2 = ObserverStub()
        event.registerObserver(observer.event1Fired)
        event.registerObserver(observer2.event1Fired)
        self.assertEqual(2, len(event._observers))

        event.unregisterObserver(observer2.event1Fired)
        self.assertEqual(1, len(event._observers))
        self.assertTrue(observer.event1Fired in event._observers)

        event.unregisterObserver(observer.event1Fired)
        self.assertEqual(0, len(event._observers))

        with self.assertRaisesRegex(
            ObservableEventError, 'not an observer of this event'
        ):
            event.unregisterObserver(observer2.event1Fired)

    def test__notifyObservers(self):
        event = ObservableEvent()
        observer = ObserverStub()
        event.registerObserver(observer.event1Fired)

        self.assertEqual(0, observer.notified_cnt)
        self.assertIsNone(observer.received_vals)

        event('arg1val', 2)

        self.assertEqual(1, observer.notified_cnt)
        self.assertEqual(('arg1val', 2), observer.received_vals)

